module bitbucket.org/uwaploe/dparchiver

go 1.21.5

require (
	bitbucket.org/mfkenney/go-redisq/v3 v3.2.1
	bitbucket.org/uwaploe/dpipc v0.1.0
	github.com/alicebob/miniredis/v2 v2.31.1
	github.com/gomodule/redigo v1.8.9
	github.com/vmihailenco/msgpack/v5 v5.4.1
)

require (
	github.com/alicebob/gopher-json v0.0.0-20200520072559-a9ecdc9d1d3a // indirect
	github.com/vmihailenco/tagparser/v2 v2.0.0 // indirect
	github.com/yuin/gopher-lua v1.1.0 // indirect
)
