// Rdsub subscribes to one or more Redis pub-sub channels and delivers the
// messages via a Go channel.
package rdsub

import (
	"context"

	"log/slog"

	"github.com/gomodule/redigo/redis"
)

// MsgRcvr is the message handler for ReceiveMsgs. It returns true if
// ReceiveMsgs should stop receiving.
type MsgRcvr func(msg redis.Message) bool

// ReceiveMsgs receives messages from psc and passes them to f, it returns
// when ctx is canceled, when the connection subscription count drops to
// zero, or when f returns true.
func ReceiveMsgs(ctx context.Context, psc redis.PubSubConn, f MsgRcvr,
	lg *slog.Logger) error {

	for {
		switch v := psc.ReceiveContext(ctx).(type) {
		case redis.Message:
			if f(v) {
				// Unsubscribing from all channels will force
				// a return.
				_ = psc.Unsubscribe()
			}
		case redis.Subscription:
			if lg != nil {
				lg.Info("subscribe",
					slog.String("kind", v.Kind),
					slog.String("channel", v.Channel),
					slog.Int("count", v.Count))
			}
			if v.Count == 0 {
				return nil
			}
		case error:
			return v
		}
		select {
		case <-ctx.Done():
			return ctx.Err()
		default:
		}
	}
}
