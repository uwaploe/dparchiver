package rdsub

import (
	"context"
	"errors"
	"testing"
	"time"

	"log/slog"

	"github.com/alicebob/miniredis/v2"
	"github.com/gomodule/redigo/redis"
)

func TestBasic(t *testing.T) {
	s := miniredis.RunT(t)
	conn, err := redis.Dial("tcp", s.Addr())
	if err != nil {
		t.Fatal(err)
	}
	psc := redis.PubSubConn{Conn: conn}

	pub, err := redis.Dial("tcp", s.Addr())
	if err != nil {
		t.Fatal(err)
	}

	var msg redis.Message

	ctx := context.Background()
	if err := psc.Subscribe("commands"); err != nil {
		t.Fatal(err)
	}

	ch := make(chan error)
	go func() {
		ch <- ReceiveMsgs(ctx, psc, func(m redis.Message) bool {
			msg = m
			return true
		}, slog.Default())
	}()

	sent := "42"
	for {
		n, _ := redis.Int(pub.Do("PUBLISH", "commands", []byte(sent)))
		if n == 1 {
			break
		}
		time.Sleep(time.Second)
	}
	err = <-ch
	if err != nil {
		t.Fatal(err)
	}

	if got := string(msg.Data); got != sent {
		t.Errorf("Bad value; expected %q, got %q", sent, got)
	}
}

func TestTimeout(t *testing.T) {
	s := miniredis.RunT(t)
	conn, err := redis.Dial("tcp", s.Addr())
	if err != nil {
		t.Fatal(err)
	}
	psc := redis.PubSubConn{Conn: conn}

	pub, err := redis.Dial("tcp", s.Addr())
	if err != nil {
		t.Fatal(err)
	}

	var msg redis.Message

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()
	if err := psc.Subscribe("commands"); err != nil {
		t.Fatal(err)
	}

	ch := make(chan error)
	go func() {
		ch <- ReceiveMsgs(ctx, psc, func(m redis.Message) bool {
			msg = m
			return false
		}, slog.Default())
	}()

	sent := "42"
	for {
		n, _ := redis.Int(pub.Do("PUBLISH", "commands", []byte(sent)))
		if n == 1 {
			break
		}
		time.Sleep(time.Second)
	}
	err = <-ch
	if !errors.Is(err, context.DeadlineExceeded) {
		t.Errorf("Unexpected error: %v", err)
	}

	if got := string(msg.Data); got != sent {
		t.Errorf("Bad value; expected %q, got %q", sent, got)
	}
}
