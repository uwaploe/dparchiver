# Deep Profiler Data Archive Manager

This program manages the data archive on both the Deep Profiler Controller (DPC) and the Docking Station Controller (DSC). It receives sensor and engineering data records and stores them in a series of files organized by time and profile number. It requires the [Redis](https://redis.io) data-store server for communication with other processes on the system.

## Archive Layout

The top-level archive directory is specified as a command-line argument. Under this directory will be a separate sub-directory for each profile. These per-profile directories use the following naming convention:

```
dataset_YYYYmmddHHMMSS_N
```

The date-time component is the starting time of the profile and *N* is the profile number.

Within each directory is a separate file for each sensor. Each file is a sequence of Message Pack format data records. Every record is a three element array:

| Index | Description                                     |
|:----- |:----------------------------------------------- |
| 0     | sample time-stamp in seconds since 1/1/1970 UTC |
| 1     | microsecond part of the timestamp               |
| 2     | sensor data sample                              |

The data sample is either a "map" (or "dictionary") associating variable names with values (the values may be integer, floating-point, or string) or a raw binary string.

## Operations

This program is run as a service by [Runit](http://smarden.org/runit/index.html) and is automatically restarted when it exits.

The program subscribes to the *events.mmp* [Redis Pubsub](https://redis.io/topics/pubsub) channel and listens for *profile:start* messages. Upon receiving a message, it creates a new archive directory and creates a new "archiver task" (the previous task, if any, is halted).
