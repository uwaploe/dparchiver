#!/usr/bin/env bash

BINDIR="$HOME/bin"
SVCDIR="$HOME/sv"
RUNDIR="$HOME/service"
PROGDIR="$(pwd)"

set -e
reqs=()
for req in "${reqs[@]}"; do
    type "$req"
done

# Install programs
files=(*)
for f in "${files[@]}"; do
    # skip directories
    [[ -d $f ]] && continue
    # skip non-executable files
    [[ -x $f ]] || continue
    ln -v -s -f "$PROGDIR/$f" "$BINDIR"
done

declare -A states

# Install runit service directory trees
if [[ -d runit ]]; then
    files=(runit/*)
    for f in "${files[@]}"; do
        # Only copy directories
        [[ -d $f ]] || continue
        svc="${f##*/}"
        if [[ -h "$RUNDIR/$svc" ]] && [[ -e "$RUNDIR/$svc/supervise/stat" ]]; then
            read stat < "$RUNDIR/$svc/supervise/stat"
            states[$svc]="$stat"
            sv stop "$RUNDIR/$svc"
            cp -av "$f" $SVCDIR
        else
            cp -av "$f" $SVCDIR
        fi
    done
    # Restart any services that were running. We do this after installation
    # in case there are dependencies between the services.
    for sv in "${!states[@]}"; do
        [[ "${states[$sv]}" = "run" ]] && sv start "$RUNDIR/$sv"
    done
fi
