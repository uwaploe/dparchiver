package main

import (
	"context"

	"log/slog"

	redisq "bitbucket.org/mfkenney/go-redisq/v3"
	"bitbucket.org/uwaploe/dparchiver/internal/rdsub"
	"bitbucket.org/uwaploe/dpipc"
	"github.com/gomodule/redigo/redis"
)

// Pass all received Event messages to a Go channel.
func eventReader(ctx context.Context, psc redis.PubSubConn) <-chan dpipc.Event {
	ch := make(chan dpipc.Event, 1)

	go func() {
		defer close(ch)
		//nolint:errcheck
		rdsub.ReceiveMsgs(ctx, psc, func(m redis.Message) bool {
			ev := dpipc.ParseEvent(string(m.Data))
			select {
			case ch <- ev:
			default:
				slog.Warn("message dropped",
					slog.String("contents", string(m.Data)))
			}
			return false
		}, slog.Default())
	}()

	return ch
}

// Write data records from a Redis queue to a channel. The records are
// unpacked to extract the sensor name then the remaining fields are
// repacked for storage in the archive file.
func dataReader(ctx context.Context, dataq *redisq.Queue[dpipc.DataRecord]) <-chan dpipc.DataRecord {
	ch := make(chan dpipc.DataRecord, 1)
	go func() {
		defer close(ch)
		for ctx.Err() == nil {
			rec, err := dataq.GetBlock(ctx)
			if err != nil {
				slog.Error("dataq Get", slog.Any("err", err))
				continue
			}
			ch <- rec
		}
	}()

	return ch
}
