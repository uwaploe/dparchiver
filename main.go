// Data archiving service for the Deep Profiler
package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"os/signal"
	"path/filepath"
	"runtime"
	"sync"
	"syscall"
	"time"

	"log/slog"

	redisq "bitbucket.org/mfkenney/go-redisq/v3"
	"bitbucket.org/uwaploe/dpipc"
	"github.com/gomodule/redigo/redis"
	msgpack "github.com/vmihailenco/msgpack/v5"
)

const Usage = `Usage: dparchiver [options] directory

Archive the data collected by Deep Profiler sensors. Data records are supplied to
the archiver by a queue implemented as a Redis list.

`

const sensorDataTable = "sensors.last"

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	rdAddr    string = "localhost:6379"
	dqName    string = "archive:queue"
	eventChan string = "events.mmp"
	evLog     bool
)

type Datarec struct {
	Sensor string
	Data   []byte
}

func initLogger() *slog.Logger {
	opts := slog.HandlerOptions{}
	// Drop log timestamps when running under Systemd
	if _, ok := os.LookupEnv("JOURNAL_STREAM"); ok {
		opts.ReplaceAttr = func(groups []string, a slog.Attr) slog.Attr {
			if a.Key == slog.TimeKey {
				return slog.Attr{}
			}
			return a
		}
	}

	return slog.New(slog.NewTextHandler(os.Stderr, &opts))
}

func abort(msg string, err error, args ...any) {
	newargs := append([]any{slog.Any("err", err)}, args...)
	slog.Error(msg, newargs...)
	os.Exit(1)
}

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprint(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.StringVar(&rdAddr, "addr", rdAddr, "Redis host:port")
	flag.StringVar(&dqName, "queue", dqName,
		"Name of Redis list for the data queue")
	flag.StringVar(&eventChan, "channel", eventChan,
		"Name of Redis channel for profile event messages")
	flag.BoolVar(&evLog, "evlog", evLog, "Log Profiler event messages to the archive")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

// Generate a unique archive name based on the current time
// and the DP profile number.
func generateName(pnum int64) string {
	t := time.Now().UTC()
	s := t.Format("20060102T150405")
	return fmt.Sprintf("%s_%d", s, pnum)
}

// Reformat an Event as a data record
func evRecord(ev dpipc.Event) dpipc.DataRecord {
	return dpipc.DataRecord{Src: "evlogger", T: time.Now(), Data: ev.String()}
}

// Convert a data queue entry to a data file record
func mkFileRec(rec dpipc.DataRecord) ([]byte, error) {
	nsecs := rec.T.UnixNano()
	secs := nsecs / 1e9
	usecs := (nsecs % 1e9) / 1e3
	data := []any{int32(secs), int32(usecs), rec.Data}
	return msgpack.Marshal(data)
}

func storeLast(conn redis.Conn, dr dpipc.DataRecord) error {
	b, err := msgpack.Marshal(dr)
	if err != nil {
		return fmt.Errorf("msgpack encode: %w", err)
	}

	_, err = conn.Do("HSET", sensorDataTable, dr.Src, b)
	return err
}

// Read DP sensor data records from a channel and write them to a file. A
// separate file is created for each sensor. Exits when the Context is
// canceled.
func archiver(ctx context.Context, cdata <-chan dpipc.DataRecord,
	conn redis.Conn, dir string, name string) {

	var (
		f        *os.File
		present  bool
		filename string
		err      error
	)

	slog.Info("archiver thread starting", slog.String("dir", dir))
	//nolint:errcheck
	conn.Do("PUBLISH", "events.archive",
		fmt.Sprintf("archive:open dir=%s", dir))

	files := make(map[string]*os.File)

loop:
	for {
		select {
		case <-ctx.Done():
			break loop
		case dr := <-cdata:
			f, present = files[dr.Src]
			if !present {
				filename = fmt.Sprintf("%s_%s.mpk", dr.Src, name)
				f, err = os.Create(filepath.Join(dir, filename))
				if err != nil {
					slog.Error("file open", slog.Any("err", err),
						slog.String("path", filename))
					continue
				}

				slog.Info("new file", slog.String("path", filename))
				files[dr.Src] = f
				defer f.Close()
			}

			b, err := mkFileRec(dr)
			if err != nil {
				slog.Error("encoding failed", slog.Any("err", err),
					slog.String("path", filename))
				continue
			}

			_, err = f.Write(b)
			if err != nil {
				slog.Error("write failed", slog.Any("err", err),
					slog.String("path", filename))
			}
			// Write the most recent sensor record to a Redis hash table
			if err := storeLast(conn, dr); err != nil {
				slog.Warn("store sensor value", slog.Any("err", err),
					slog.String("sensor", dr.Src))
			}
		}
	}
	slog.Info("archiver thread exiting", slog.String("dir", dir))
	//nolint:errcheck
	conn.Do("PUBLISH", "events.archive",
		fmt.Sprintf("archive:close dir=%s", dir))
}

func main() {
	args := parseCmdLine()
	if len(args) < 1 {
		flag.Usage()
		os.Exit(1)
	}
	topdir := args[0]

	slog.SetDefault(initLogger())

	// Message publishing connection
	conn, err := redis.Dial("tcp", rdAddr)
	if err != nil {
		abort("Redis connect failed", err)
	}

	// Pub-sub consumer connection
	psconn, err := redis.Dial("tcp", rdAddr)
	if err != nil {
		abort("Redis connect failed", err)
	}
	psc := redis.PubSubConn{Conn: psconn}

	dqconn, err := redis.Dial("tcp", rdAddr)
	if err != nil {
		abort("Redis connect failed", err)
	}
	// Input data queue implemented as a Redis list
	dataq := redisq.NewQueue[dpipc.DataRecord](dqconn, dqName, 0)

	var (
		dir             string
		name            string
		ok              bool
		wg              sync.WaitGroup
		pnum            int64
		ctx, pctx       context.Context
		cancel, pcancel context.CancelFunc
	)

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	// Initialize the top-level Context
	ctx, cancel = context.WithCancel(context.Background())
	defer cancel()

	// Goroutine to wait for signals or for the signal channel
	// to close.
	go func() {
		s, more := <-sigs
		if more {
			slog.Info("interrupt", slog.String("sig", s.String()))
			psc.Unsubscribe() //nolint:errcheck
			cancel()
		}
	}()

	cdata := make(chan dpipc.DataRecord, 1)
	drchan := dataReader(ctx, dataq)
	evchan := eventReader(ctx, psc)

	if err := psc.Subscribe(eventChan); err != nil {
		slog.Error("Redis subscribe", slog.Any("err", err),
			slog.String("chan", eventChan))
	}

	slog.Info("DP archiver", slog.String("vers", Version))

loop:
	for {
		select {
		case <-ctx.Done():
			break loop
		case ev := <-evchan:
			if ev.Name == "profile:start" {
				// Stop the currently running archiver goroutine and
				// start a new one.
				if pctx != nil {
					pcancel()
					slog.Info("Waiting for thread to exit")
					wg.Wait()
				}
				pnum, ok = ev.Attrs["pnum"].(int64)
				if !ok {
					pnum = 0
				}
				name = generateName(pnum)
				dir = filepath.Join(topdir, "dataset_"+name)
				err = os.MkdirAll(dir, os.ModeDir|0755)
				if err != nil {
					slog.Error("mkdir failed", slog.Any("err", err))
				} else {
					pctx, pcancel = context.WithCancel(ctx)
					wg.Add(1)
					go func() {
						defer wg.Done()
						archiver(pctx, cdata, conn, dir, name)
					}()
				}
			}

			if evLog {
				dr := evRecord(ev)
				select {
				case cdata <- dr:
				default:
				}
			}
		case dr := <-drchan:
			select {
			case cdata <- dr:
			default:
			}
		}
	}

	if pctx != nil {
		pcancel()
		slog.Info("Waiting for thread to exit")
		wg.Wait()
	}
}
